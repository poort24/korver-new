function QuickOrder(el, base_url) {
    this.element = el;
    this.number = 0;

    this.base_url = base_url;
    this.base_url_product = this.base_url + 'poort24_quickorder/product/';

    this.deleteText = '';
    this.deleteImageUrl = '';

    this.busyUploading = false;

    this.loader = jQuery('.loader');

    this.showLoader = function () {
        this.loader.show();
    };

    this.hideLoader = function () {
        this.loader.hide();
    };

    this.addOrderline = function (row) {
        var newline = this.getOrderline(row);
        jQuery(this.element + ' table tbody').append(newline);
    };

    this.getOrderline = function (row) {
        var sku = '';
        var qty = '1';
        var baseprice = '';
        var price = '';
        var discount = '';
        var title = '';
        var popup = '';
        var image = '';
        var productUrl = '';
        var action = '';

        if (row) {
            sku = row[0];
            qty = row[1];
            json = jQuery.parseJSON(row[2]);
            if (json) {
                title = json[1];
                price = json[3];
                baseprice = json[4];
                discount = json[5];
                popup = json[6];
                image = json[7];
                productUrl = json[8];
                action = json[9];
            }
        }
        if (image != '') {
            image = "<img src='" + image + "' height='60' />"
        }

        if (action) {
            action = "<b>" + Translator.translate('Special price') + "</b><br />"
        }

        var errorTemplate = jQuery('#order-line-error-template').html();
        var newline = '';

        if (jQuery('.product_price').length) {
            newline += "<tr id='quick-order-" + this.number + "'>" +
                "<td class='td-sku'><input class='sku' type='' name='quickorder[\"" + this.number + "\"][sku]' va   ='" + sku + "' /><div class='search_autocomplete'></div></td>" +
                "<td class='td-qty'><input class='qty' type='' name='quickorder[\"" + this.number + "\"][qty]' value='" + qty + "' /><span class='popup'>" + popup + "</span></td>" +
                "<td class='product_image'>" + image + "</td>" +
                "<td class='product_title'>" +
                "<div class='title'><a href='" + productUrl + "' target='_blank'>" + title + "</a></div>" +
                "<div class='quick-order-error'>" + errorTemplate + "</div>" +
                "</td>" +
                "<td class='product_adv_price'>" + baseprice + "</td>" +
                "<td class='product_discount'>" + discount + "</td>" +
                "<td class='product_price'>" + action + price + "</td>" +
                "<td class='delete'>" +
                "<a class='button delete-order'>" +
                    "<img src='" + this.deleteImageUrl + "' alt='" + this.deleteText + "' /></span>" +
                //"X" +
                "</a>" +
                "</td>" +
                "</tr>";
        } else {
            newline = "<tr id='quick-order-" + this.number + "'>" +
                "<td><input class='sku' type='' name='quickorder[\"" + this.number + "\"][sku]' value='" + sku + "' /><div class='search_autocomplete'></div></td>" +
                "<td><input class='qty' type='' name='quickorder[\"" + this.number + "\"][qty]' value='" + qty + "' /><span class='popup'>" + popup + "</span></td>" +
                "<td class='product_image'>" + image + "</td>" +
                "<td class='product_title'>" +
                "<div class='title'><a href='" + productUrl + "' target='_blank'>" + title + "</a></div>" +
                "<div class='quick-order-error'>" + errorTemplate + "</div>" +
                "</td>" +
                "<td class='product_adv_price'>" + baseprice + "</td>" +
                "<td class='delete'>" +
                "<a class='button delete-order'>" +
                    //"<img src='" + this.deleteImageUrl + "' alt='" + this.deleteText + "' />" +
                "X" +
                "</a>" +
                "</td>" +
                "</tr>";
        }
        this.number++;
        return newline;
    };

    this.deleteOrderLine = function (el) {
        var parent = el.parents('tr');
        var url = this.base_url + 'delete';
        var sku = parent.find('.sku').val();
        var data = 'sku=' + sku;
        var me = this;

        me.showLoader();
        jQuery.ajax({
            type: 'post',
            url: url,
            data: data,
            complete: function () {
                parent.remove();
                me.hideLoader();
            }
        });
    };

    this.getProductsByInput = function (el) {
        var url = this.base_url_product + 'search';
        var parent = el.closest('tr');
        var data = parent.find('input').serialize();

        var doneTypingInterval = 300;
        var me = this;

        me.showLoader();

        return setTimeout(function () {
            jQuery.ajax({
                type: "post",
                url: url,
                data: data,
                dataType: "json",
                success: function (data) {
                    parent.find('.search_autocomplete').show();
                    if (!data || data[1] != parent.find('.sku').val() || data[0] == '') {
                        parent.find('.search_autocomplete').html("<div id=''>" + Translator.translate('No articles found') + "</div>");
                    }
                    else {
                        parent.find('.search_autocomplete').html(data[0]);
                        parent.find('.search_autocomplete div').click(function () {
                            jQuery(this).parents('tr').find('.sku').val(jQuery(this).attr('id'));
                        });
                    }
                    me.hideLoader();
                }
            });
        }, doneTypingInterval);
    };

    this.changeDescription = function (el) {
        var url = this.base_url_product + 'description';
        var parent = el.closest('tr');
        var data = parent.find('input').serialize();
        parent.find('.search_autocomplete').hide();
        var me = this;

        var loginScreenOpened = false;

        me.showLoader();

        jQuery.ajax({
            type: "post",
            url: url,
            data: data,
            dataType: "json",
            complete: function() {
                me.hideLoader();
            },
            success: function (data) {
                var over_stock_span = parent.find('.quick-order-over-stock');
                var over_stock_qty_span = parent.find('.quick-order-stock-no');
                parent.removeClass('quick-order-not-available');
                parent.removeClass('quick-order-not-for-sale');
                parent.removeClass('quick-order-not-in-stock');
                over_stock_span.hide();
                over_stock_qty_span.hide();
                parent.removeClass('quick-order-delivery-date');
                parent.removeClass('quick-order-in-stock');
                parent.removeClass('quick-order-delivery-date-inform');

                if (!data) {
                    return;
                }

                if (data[0] == 'error' && data[2] == 'no session' && !loginScreenOpened) {
                    loginScreenOpened = true;
                    alert(Translator.translate('Your browser session has expired. You have to login again. You will be redirected in a new window. Login again and return to this screen by closing the new window. You can now continue your order.'));
                    window.open("/customer/account/login/", "_blank");
                    setTimeout(function () {
                        loginScreenOpened = false;
                    }, 10 * 3600);
                    return;
                }

                var type = data[0];
                var title = data[1];
                var status = data[2];
                var price = data[3];
                var baseprice = data[4];
                var korting = data[5];
                var popup = data[6];
                var image = data[7];
                var productUrl = data[8];
                var action = data[9];
                var delivery_date = data[10];

                if (delivery_date != null) {
                    var day = delivery_date.substring(8, 10);
                    var month = delivery_date.substring(5, 7);
                    var year = delivery_date.substring(0, 4);
                    var date = day + '-' + month + '-' + year;
                }

                var popupElem = parent.find('.popup').empty().hide();
                if (type == 'title') {
                    if (image != '') {
                        image = "<img src='" + image + "' height='60' />"
                    }

                    if (action) {
                        action = "<b>" + Translator.translate('Special price') + "</b><br />"
                    } else {
                        action = '';
                    }

                    parent.find('.product_image').html(image);
                    parent.find('.product_title .title').html('<a href="' + productUrl + '" target="_blank">' + title + '</a>');
                    parent.find('.product_price').html(action + price);
                    parent.find('.product_adv_price').html(baseprice);
                    parent.find('.product_discount').html(korting);
                    if (popup) {
                        popupElem.html(popup).show();
                    }
                    if (!status.in_stock) {
                        parent.addClass('quick-order-not-in-stock');
                        if (delivery_date != null) {
                            parent.addClass('quick-order-delivery-date');
                            parent.find('.de_date').html(date + " <span style='display:inline-block!important;'>*</span>&nbsp;");
                        }
                        else {
                            parent.addClass('quick-order-delivery-date-inform');
                            parent.find('.de_date').html(Translator.translate('Ask for the delivery time.') + '&nbsp;');
                        }
                    } else if (status.over_stock) {
                        over_stock_span.show();
                        over_stock_qty_span.show();
                        if (delivery_date != null) {
                            parent.addClass('quick-order-delivery-date');
                            parent.find('.de_date').html(date + " <span style='display:inline-block!important;'>*</span>&nbsp;");
                        }
                        else {
                            parent.addClass('quick-order-delivery-date-inform');
                            parent.find('.de_date').html(Translator.translate('Ask for the delivery time.') + '&nbsp;');
                        }
                    }
                    else {
                        parent.addClass('quick-order-in-stock');
                    }
                    parent.find('.quick-order-stock-no').text(parseInt(status.stock_qty));
                }

                if (type == 'error' && status == 'not for sale') {
                    parent.find('.producttitle .title').html(title);
                    parent.addClass('quick-order-not-for-sale');
                }

                if (type == 'error' && status == 'not available') {
                    parent.addClass('quck-order-not-available');
                }

                if (type == 'error' && status == 'bundle product') {
                    parent.find('.producttitle .title').html('<a href="' + price + '" target="_blank">' + title + '</a>');
                    parent.addClass('quick-order-error-bundle-product');
                }

                me.hideLoader();
            }
        });
    };

    this.setBusyUploading = function (busyUploading) {
        this.busyUploading = busyUploading;
    };

    this.submitForm = function (el, event) {
        // 'me' can be used in ajax complete and success ('this' can't)
        var me = this;
        var url = me.base_url_product + 'check';
        var data = el.serialize();

        var orderbtn = jQuery('.order');
        var quickOrderTable = jQuery('.quick-order');

        if (data != '') {
            if (me.busyUploading) {
                event.preventDefault();
                return false;
            }

            me.setBusyUploading(true);
            // Hide order button
            orderbtn.hide();

            var busyRedirecting = false;

            me.showLoader();

            jQuery.ajax({
                type: "post",
                url: url,
                data: data,
                timeout: 10000,
                dataType: "json",
                complete: function () {
                    if (!busyRedirecting) {
                        me.setBusyUploading(false);
                        orderbtn.show();
                    }
                    me.hideLoader();
                },
                success: function (data) {
                    quickOrderTable.find('tr').removeClass('quick-order-not-available');

                    var notAvailable = false;

                    jQuery.each(data, function () {
                        var row = this[0];
                        var status = this[1];

                        if (status == 'order is placed') {
                            /** disabled deletion of rows for now **/
                            quickOrderTable.find('table tbody tr').remove();
                            busyRedirecting = true;
                            window.location.replace(me.base_url + 'checkout/cart');
                        }

                        if (status == 'not for sale') {
                            jQuery('#quick-order-' + row).addClass('quick-order-not-for-sale');
                            notAvailable = true;
                        }

                        if (status == 'not available') {
                            jQuery('#quick-order-' + row).addClass('quick-order-not-available');
                            notAvailable = true;
                        }
                    });

                    if (notAvailable) {
                        alert(Translator.translate("Your order contains article numbers that we don't offer or that are not available. Check your order and try again."));
                        event.preventDefault();
                    }
                }
            });
        }
        event.preventDefault();
        return false;
    };

    this.setDeleteImageUrl = function (image) {
        this.deleteImageUrl = image;
    };

    this.setDeleteText = function (text) {
        this.deleteText = text;
    };
}