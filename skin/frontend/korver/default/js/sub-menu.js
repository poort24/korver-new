/**
 * Created by bschut on 03-08-16.
 */

jQuery(document).ready(function () {
    jQuery('.cat-toggler').click(function (e) {
        e.preventDefault();
        jQuery(this).parent('li').children('ul').children('li').toggle();
            jQuery(this).find('.toggler-plus').toggle();
            jQuery(this).find('.toggler-min').toggle();
    });

    // Show active menu list item and siblings
    jQuery('#navigation-left-wrapper').find('.active').parent('li').css('display', 'list-item');
    jQuery('#navigation-left-wrapper').find('.active').parent('li').siblings('li').css('display', 'list-item');

    // Show parent of active menu item
    var catParentList = jQuery('#navigation-left-wrapper').find('.active').parents('.cat-parent');
    jQuery(catParentList).each(function(index) {
        if(index == catParentList.length - 1) {
            jQuery(this).children('ul').children('li').css('display', 'list-item');
        } else {
            jQuery(this).css('display', 'list-item');
        }
        //console.log(this);
        jQuery(this).children('.cat-toggler').find('svg').toggle();
    });
});