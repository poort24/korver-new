/**
 * Created by bschut on 03-08-16.
 */
jQuery(document).ready(function () {
    jQuery('#description-read-more').click(function(e) {
        e.preventDefault();
        if(jQuery(this).text() == "Lees meer") {
            jQuery(this).text("Lees minder");
        } else {
            jQuery(this).text("Lees meer");
        }
        jQuery(".description-more-text").toggleClass('description-hide');
    });
});