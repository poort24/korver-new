jQuery(function() {
    jQuery('.toggle-nav').click(function() {
        toggleNav();
    });
});

function toggleNav() {
    if (jQuery('.wrapper').hasClass('show-nav')) {
        jQuery('.wrapper').removeClass('show-nav');
        jQuery('#overlay').fadeOut();
    } else {
        jQuery('.wrapper').addClass('show-nav');
        jQuery('#overlay').fadeIn();
        jQuery('.skip-mobile-nav').css('background', 'rgba(1, 23, 44, 0.5)');
    }
}

jQuery( document ).ready(function() {
    jQuery( "#product-group-link" ).click(function() {
        jQuery('#nav-dropdown').toggle();
        jQuery('#nav-dropdown-2').toggle();
    });

    jQuery('#mobile-cat-triangle-down').click(function() {
        jQuery(this).toggle();
        jQuery('#mobile-cat-triangle-up').toggle();
    });

    jQuery('#mobile-cat-triangle-up').click(function() {
        jQuery(this).toggle();
        jQuery('#mobile-cat-triangle-down').toggle();
    });

    jQuery('#product-group-link').mouseenter(function() {
        jQuery('#mobile-cat-triangle-down').toggle();
        jQuery('#mobile-cat-triangle-up').toggle();
    });

    jQuery('#product-group-link').mouseleave(function() {
        jQuery('#mobile-cat-triangle-down').toggle();
        jQuery('#mobile-cat-triangle-up').toggle();
    });
});