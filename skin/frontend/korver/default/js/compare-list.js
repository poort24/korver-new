jQuery(document).ready(function () {
    jQuery('.add-to-compare-list').click(function (e) {
        window.location.href = jQuery(this).val();
    });

    jQuery(".category-products").find('.item').hover(
        function() {
            jQuery(this).find('.add-to-links').toggle();
        }, function() {
            jQuery(this).find('.add-to-links').toggle();
        }
    );
});