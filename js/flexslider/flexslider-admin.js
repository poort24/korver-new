solide(document).ready(function() {

	/* initial show/hide elements */
	flexsliderEnabled = solide('#flexslider_general_enabled').val();
	toggleFieldsets(flexsliderEnabled);

	/* hide or show fieldsets based on changing extension status */
	/* hide or show elements based on changing zoomtype */
	solide('#flexslider_general_enabled').change(function() {
		flexsliderEnabled = solide(this).val();
		toggleFieldsets(flexsliderEnabled);
	});

	solide('.colorpicker').spectrum({
		showInput: true,
		showAlpha: true,
		showInitial: true,
		showInput: true,
		showButtons: false,
		clickoutFiresChange: true,
		preferredFormat: "rgb",
	});

});

/* function for showing or hiding all fieldsets based on if extension is enabled */
function toggleFieldsets(status){
	switch(status){
		case '0':
			solide('#flexslider_advanced_settings').closest('.section-config').hide();
			break;
		case '1':
			solide('#flexslider_advanced_settings').closest('.section-config').show();
			break;
	}
}
