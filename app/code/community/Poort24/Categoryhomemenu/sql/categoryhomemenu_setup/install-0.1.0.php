<?php

$this->startSetup();

$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_on_home', array(
    'group'             => 'General Information',
    'input'             => 'select',
    'type'              => 'int',
    'label'             => 'Show on home',
    'backend'           => '',
    'visible'           => true,
    'required'          => true,
    'visible_on_front'  => true,
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'source'            => 'eav/entity_attribute_source_boolean',
    'default'           => 0,
));

$this->endSetup();