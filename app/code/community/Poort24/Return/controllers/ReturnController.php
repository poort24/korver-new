<?php
class Poort24_Return_ReturnController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function ajaxAction() {
        $action = $this->getRequest()->getParam('action');

        switch ($action) {
            case "GetProductDescription":
                $sku = $this->getRequest()->getParam('sku');
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

                if ($product) {
                    echo $product->getName();
                }
                break;
        }
    }

    public function sendemailAction() {
        $emailResult = Mage::getModel('poort24_return/return')->sendReturnEmail($this->getRequest()->getPost());
        Mage::getSingleton('core/session')->setEmailData($emailResult);
        if ($emailResult) {
            Mage::getSingleton('catalog/session')->addSuccess($this->__('Return is succesfully added'));
            Mage::getSingleton('core/session')->setEmailSessionVar($emailResult);
        } else {
            Mage::getSingleton('catalog/session')->addError($this->__('Error during return sending'));
        }

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
        //$this->_redirect('customerextra/return/sendemailresult');
    }

    public function sendemailresultAction() {

    }
}
?>