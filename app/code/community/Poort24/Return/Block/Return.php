<?php
class Poort24_Return_Block_Return extends Mage_Core_Block_Template
{
	public $_customer;
	public $_address;

	public $_emailContent = false;

	public function __construct() {
		$this->_customer = Mage::getSingleton('customer/session')->getCustomer();

		$address_id = $this->_customer->getDefaultShipping();
		if ($address_id) {
			$this->_address = Mage::getModel('customer/address')->load($address_id);
		}

		if(Mage::getSingleton('core/session')->getEmailSessionVar()) {
			$this->_emailContent = Mage::getSingleton('core/session')->getEmailSessionVar();
		}
	}

	public function getCompany() {
		if (!empty($this->_address)) {
			return $this->_address->getCompany();
		}
	}

	public function getFirstname() {
		if (!empty($this->_address)) {
			return $this->_address->getFirstname();
		}
	}

	public function getLastname() {
		if (!empty($this->_address)) {
			return $this->_address->getLastname();
		}
	}

	public function getStreet() {
		if (!empty($this->_address)) {
			return $this->_address->getStreet1();
		}
	}

	public function getHouseNumber() {
		if (!empty($this->_address)) {
			return $this->_address->getStreet2();
		}
	}

	public function getPostcode() {
		if (!empty($this->_address)) {
			return $this->_address->getPostcode();
		}
	}

	public function getCity() {
		if (!empty($this->_address)) {
			return $this->_address->getCity();
		}
	}

	public function getPhoneNumber() {
		if (!empty($this->_address)) {
			return $this->_address->getTelephone();
		}
	}

	public function getEmail() {
		if (!empty($this->_customer)) {
			return $this->_customer->getEmail();
		}
	}
}