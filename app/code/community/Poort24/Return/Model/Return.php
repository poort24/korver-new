<?php
class Poort24_Return_Model_Return extends Mage_Core_Model_Abstract
{
	const XML_PATH_SALES_EMAIL_RETURN_IDENTITY              = 'sales_email/return/email_identity';
	const XML_PATH_SALES_EMAIL_RETURN_TEMPLATE              = 'sales_email/return/email_template';
	const XML_PATH_SALES_EMAIL_RETURN_COPY_TO              	= 'sales_email/return/email_copy_to';

	public function sendReturnEmail($data) {
		Mage::log("In functie sendReturnEmail");

		try {
			$storeId = Mage::helper('core')->getStoreId();

			$firstname = $data['firstname'];
			$lastname = $data['lastname'];
			$email = $data['email'];

			$templateId = Mage::getStoreConfig(self::XML_PATH_SALES_EMAIL_RETURN_TEMPLATE, $storeId);

			$mailer = Mage::getModel('core/email_template_mailer');
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($email, $firstname." ".$lastname);

			$mailer->addEmailInfo($emailInfo);

			$copyTo = Mage::getStoreConfig(self::XML_PATH_SALES_EMAIL_RETURN_COPY_TO, $storeId);
			foreach (explode(",", $copyTo) as $email) {
				$emailInfo = Mage::getModel('core/email_info');
				$emailInfo->addTo($email);
				$mailer->addEmailInfo($emailInfo);
			}

			$retourNumber = Mage::getStoreConfig('sales_email/return/retour_number', Mage::app()->getStore());

			$products = '';
			if(isset($data['quickorder']))
				$products = $data['quickorder'];

			$vars = array(
				'company'			=> $data['company'],
				'firstname'			=> $firstname,
				'lastname'			=> $lastname,
				'street_1'			=> $data['street_1'],
				'postcode'			=> $data['postcode'],
				'city'				=> $data['city'],
				'phonenumber'		=> $data['phonenumber'],
				'email'				=> $data['email'],
				'discussed_with'	=> $data['discussed_with'],
				'ordernumber'		=> $data['ordernumber'],
				'orderdate'			=> $data['orderdate'],
				'reason_return'		=> $data['reason_return'],
				'action'			=> $data['action'],
				'articles'			=> $this->getProductTableList($products),
				'comment'			=> $data['comment'],
				'retour_number'		=> $retourNumber
			);

			$mailer->setSender(Mage::getStoreConfig(self::XML_PATH_SALES_EMAIL_RETURN_IDENTITY, $storeId));
			$mailer->setStoreId($storeId);
			$mailer->setTemplateId($templateId);
			$mailer->setTemplateParams($vars);

			$mailer->send();

			Mage::getModel('core/config')->saveConfig('sales_email/return/retour_number', $retourNumber+1);
			Mage::app()->getCacheInstance()->flush();
			return $vars;
		} catch (Exception $e) {
			Mage::log("Error during sending return email: ".$e->getMessage());
			
			return false;
		}

		return true;
	}

	private function getProductTableList($products) {
		$list = '';

	    foreach ($products as $product) {
	    	if($product['sku'] != '') {
		    	$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $product['sku'])->getData();

		    	$list .= "<tr>
			    			  <td align='left' bgcolor='#f8f7f5' style='padding:3px 9px;border-bottom:1px dotted #CCCCCC;'>".$_product['name']."</td>
			      			  <td align='left' bgcolor='#f8f7f5' style='padding:3px 9px;border-bottom:1px dotted #CCCCCC;'>".$product['sku']."</td>
			      			  <td align='left' bgcolor='#f8f7f5' style='padding:3px 9px;border-bottom:1px dotted #CCCCCC;'>".$product['qty']."</td>
		    			  </tr>";
		 	}
	    }

	    if($list == '') {
	    	$list .= "<tr><td></td><td></td><td></td></tr>";
	    }

	    return $list;
	}
}
?>