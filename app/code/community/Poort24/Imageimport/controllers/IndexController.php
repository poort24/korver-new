<?php
class Poort24_Imageimport_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('catalog');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Catalog'), Mage::helper('adminhtml')->__('Catalog'));
        $this->_addContent($this->getLayout()->createBlock('imageimport/upload'));
        $this->renderLayout();
    }

    public function uploadAction() {
        $result = array ();
        try {
            $uploader = new Varien_File_Uploader('file');
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','zip'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);

            $path = Mage::getBaseDir('media') . DS . 'imageimport';
            if (!file_exists($path)) {
                $umask = umask(0777);
                mkdir($path, 0777);
                umask($umask);
            }
            $path .= DS;

            $uploader->save($path);

            // unzip ZIP files
            foreach (glob("{$path}*.zip") as $zip) {
                exec("unzip {$zip} -d {$path}");
                unlink($zip);
            }

        } catch (Exception $e) {
            $result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    public function saveAction() {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);
        $imageType = array();
        if($this->getRequest()->getParam('main_image')){
            $imageType = array('thumbnail','small_image','image');
        }
        if($this->getRequest()->getParam('lijntekening')){
            $imageType[] = 'lijntekening';
        }
        if($this->getRequest()->getParam('explode_view')){
            $imageType[] = 'explode_view';
        }

        if ($this->getRequest()->getPost()) {
            Mage::getModel('imageimport/importer')->import(Mage::getBaseDir('media') . DS . 'imageimport', $imageType);
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog');
    }
}