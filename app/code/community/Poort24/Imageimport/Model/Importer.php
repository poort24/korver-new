<?php
class Poort24_Imageimport_Model_Importer extends Mage_Core_Model_Abstract {
    public function import($dir, array $imageType = array()) {
        $dir = rtrim($dir, DS);
        $im = new Imagick();
        $products = Mage::getModel('catalog/product')->getCollection()->load();
        foreach ($products as $product) {
            foreach ($this->getImageCodes($product) as $imageCode) {
                foreach (glob("{$dir}/*{$imageCode}*") as $imgSrc) {
                    $copySrc = Mage::getBaseDir('media') . '/import/copies/_' . $product->getSku() . '.jpg';
                    if (file_exists($copySrc)) {
                        unlink($copySrc);
                        continue 3; // next product
                    }
                    copy($imgSrc, $copySrc);
                    $im->pingImage($imgSrc);
                    $im->readImage($imgSrc);
                    $im->thumbnailImage( 1000, null );
                    $im->thumbnailImage( null, 1000 );
                    $im->writeImage($copySrc);

                    $product
                        ->setMediaGallery(array ('images' => array (), 'values' => array ()))
                        ->addImageToMediaGallery($copySrc, $imageType, false, false)
                        ->save();

                    unlink($copySrc);

                    Mage::getModel('adminhtml/session')->addSuccess("Afbeelding geimporteerd voor product {$product->getSku()}");

                    continue 3; // next product
                }
            }
        }

        foreach (glob("{$dir}/*") as $file) {
            if (is_file($file) && is_writable($file)) {
                unlink($file);
            }
        }

        Mage::getModel('adminhtml/session')->addSuccess("Klaar met import afbeeldingen");
    }

    public function getImageCodes($product) {
        return array ($product->getSku());
    }
}