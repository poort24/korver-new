<?php
class Poort24_Imageimport_Block_Upload_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
            )
        );


        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('edit_form', array('legend'=>'Options'));

        $fieldset->addField('main_image', 'checkbox', array(
            'label'     => 'Hoofdafbeelding',
            'name'      => 'main_image',
            'checked' => false,
            'value'  => 'main_image',
            'disabled' => false,
            'tabindex' => 1
        ));

        $fieldset->addField('lijntekening', 'checkbox', array(
            'label'     => 'Lijntekening',
            'name'      => 'lijntekening',
            'checked' => false,
            'value'  => 'lijntekening',
            'disabled' => false,
            'tabindex' => 1
        ));

        $fieldset->addField('explode_view', 'checkbox', array(
            'label'     => 'Explode view',
            'name'      => 'explode_view',
            'checked' => false,
            'value'  => 'explode_view',
            'disabled' => false,
            'tabindex' => 1
        ));

        return parent::_prepareForm();
    }
}
