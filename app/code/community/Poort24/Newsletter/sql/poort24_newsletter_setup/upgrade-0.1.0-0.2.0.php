<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('newsletter_queue_group_link'))
    ->addColumn('link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Link ID')
    ->addColumn('queue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 7, array(
        'nullable' => false,
        'unsigned' => true,
        'default' => 0
    ), 'Queue ID')
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 5, array(
        'nullable' => false,
        'unsigned' => true,
        'default' => 0
    ), 'Group ID')
    ->addIndex('group_id', 'group_id')
    ->addIndex('queue_id', 'queue_id');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('newsletter_queue_user_link'))
    ->addColumn('link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Link ID')
    ->addColumn('queue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 7, array(
        'nullable' => false,
        'unsigned' => true,
        'default' => 0
    ), 'Queue ID')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 1, array(
        'nullable' => false
    ), 'Type')
    ->addColumn('user', Varien_Db_Ddl_Table::TYPE_TEXT, 16, array(
        'nullable' => true
    ), 'User')
    ->addIndex('user', 'user')
    ->addIndex('queue_id', 'queue_id');
$installer->getConnection()->createTable($table);

$installer->endSetup();