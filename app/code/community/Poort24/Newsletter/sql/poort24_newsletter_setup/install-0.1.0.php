<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_choice', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 30,
        'comment' => 'Template Design Choice'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_products', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Template Products'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_content1', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'comment' => 'Template Intro Text'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_content2', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'comment' => 'Template Closing Text'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_content3', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'comment' => 'Template Text Sidebar'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'template_description1', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'comment' => 'Template Text Actionpage'
    ));

$installer->getConnection()
    ->addColumn('newsletter_template', 'newsletter_expires', array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable' => true,
        'comment' => 'Newsletter Expires'
    ));

$installer->endSetup();