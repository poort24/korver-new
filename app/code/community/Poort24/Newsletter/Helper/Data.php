<?php

class Poort24_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getNewsletters($customerId = null)
    {
        $output = array();
        if ($customerId = $this->_getCustomerId($customerId)) {
            $templates = Mage::getModel('newsletter/template')->getCollection();
            foreach ($templates as $template) {
//                $template = $template->load($template->getId());
                $expires = $template->getNewsletterExpires();
                if ($expires && strtotime($expires) + 24 * 3600 < time()) {
                    // expired
                    continue;
                }
                if (!file_exists($this->getNewsletterPath($template->getId(), $customerId))) {
                    continue;
                }
                $output[$template->getId()] = array(
                    'title' => ($title = $template->getTemplateSubject()) ? $title : 'Nieuwsbrief',
                    'url' => '/media' . $this->_getNewsletterMediaPath($template->getId(), $customerId),
                    'description' => $template->getTemplateDescription1(),
                );
            }
        }

        ksort($output);
        $output = array_reverse($output);
        return $output;
    }

    public function getNewsletterLinks()
    {
        if ($newsletters = $this->getNewsletters()) {
            echo "<p>Lees online onze nieuwsbrief:</p>\n";
            echo "<ul class=\"newsletters\">\n";
            foreach ($newsletters as $newsletter) {
                echo "<li><a href=\"" . htmlspecialchars($newsletter['url']) . "\" target=\"_blank\">" . htmlspecialchars($newsletter['title']) . "</a>" . ($newsletter['description'] ? ": " . htmlspecialchars($newsletter['description']) : '') . '</li>';
            }
            echo "</ul>\n";
        }
    }

    public function writeNewsletter($text, $template, $mail, $subscriber)
    {
        if ($customerId = $subscriber->getCustomerId()) {
            $path = $this->getNewsletterPath($template->getId(), $customerId);

            Mage::log($path, null, 'default.log');
            $this->checkDirectory($path);

            $text = str_replace("<body", "<body template=\"" . $template->getId() . "\"", $text);
            @file_put_contents($path, "<html><title>" . $mail->getSubject() . "</title>" . $text . "</html>");
        }
    }

    public function getNewsletterPath($templateId, $customerId)
    {
        return Mage::getBaseDir('media') . $this->_getNewsletterMediaPath($templateId, $customerId);
    }

    protected function _getNewsletterMediaPath($templateId, $customerId)
    {
        return '/newsletters/' . $templateId . '/' . md5('korver@#$*n' . $templateId . '/' . $customerId) . '.html';
    }

    protected function _getCustomerId($customerId)
    {
        if ($customerId) {
            return $customerId;
        }
        if ($customer = Mage::getSingleton('customer/session')->getCustomer()) {
            return $customer->getId();
        }
    }

    public function getImageUrl($product)
    {
        $productId = $product->getId();

        $imagePath = Mage::getBaseDir('media') . $this->_getNewsletterImagePath($productId);
        $imageUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->_getNewsletterImageUrl($productId);

        if (file_exists($imagePath) === true && is_file($imagePath) === true) {
            return $imageUrl;
        }

        $cacheUrl = Mage::helper('catalog/image')->init($product, 'small_image')->resize(135);

        $position = stripos($cacheUrl, 'catalog');
        $path = Mage::getBaseDir('media') . substr($cacheUrl, ($position - 1));

        $this->checkDirectory($path);

        copy($path, $imagePath);

        return $imageUrl;
    }

    protected function _getNewsletterImagePath($productId)
    {
        return '/' . $this->_getNewsletterImageUrl($productId);
    }

    protected function _getNewsletterImageUrl($productId)
    {
        return 'catalog/product/newsletter/' . md5('korver@#$*n' . $productId) . '.jpg';
    }

    protected function checkDirectory($path)
    {
        if (!file_exists(dirname($path))) {
            $umask = umask(0);
            mkdir(dirname($path), 0777, true);
            umask($umask);
        }
    }

    public function getTemplates()
    {
        return array(
            'korverholland' => 'Korver Holland',
        );
    }

    public function getDefaultContent()
    {
        return '<p style="font-size:18px; color:#49AB3E; font-weight:bold;">Titel</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut nunc id lectus suscipit vestibulum. Ut nec justo ligula, in rutrum nisl. Donec eu erat ipsum, quis varius dolor. Nulla non urna ipsum, eget fermentum massa. Aliquam erat volutpat. Curabitur.</p>';
    }

}