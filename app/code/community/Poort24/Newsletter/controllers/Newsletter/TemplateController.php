<?php
include Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Newsletter' . DS . 'TemplateController.php';

class Poort24_Newsletter_Newsletter_TemplateController extends Mage_Adminhtml_Newsletter_TemplateController
{

    public function saveAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/newsletter_template'));
        }
        $template = Mage::getModel('newsletter/template');

        if ($id = (int)$request->getParam('id')) {
            $template->load($id);
        }

        try {
            $products = $request->getParam('products', array());
            $products = preg_split('/[\n\r]+/', $products);

            $template->addData($request->getParams())
                ->setTemplateSubject($request->getParam('subject'))
                ->setTemplateCode($request->getParam('code'))
                ->setTemplateSenderEmail($request->getParam('sender_email'))
                ->setTemplateSenderName($request->getParam('sender_name'))
                ->setTemplateText($request->getParam('text'))
                ->setTemplateStyles($request->getParam('styles'))
                ->setModifiedAt(Mage::getSingleton('core/date')->gmtDate())
                ->setTemplateProducts($products);

            if ($expires = $request->getParam('newsletter_expires')) {
                $template->setNewsletterExpires(date('Y-m-d', strtotime($expires)));
            }

            if (!$template->getId()) {
                $template->setTemplateType(Mage_Newsletter_Model_Template::TYPE_HTML);
            }
            if ($this->getRequest()->getParam('_change_type_flag')) {
                $template->setTemplateType(Mage_Newsletter_Model_Template::TYPE_TEXT);
            }
            if ($this->getRequest()->getParam('_save_as_flag')) {
                $template->setId(null);
            }

            $template->save();
            $this->_redirect('*/*');
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError(nl2br($e->getMessage()));
            $this->_getSession()->setData('newsletter_template_form_data', $this->getRequest()->getParams());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, Mage::helper('adminhtml')->__('Error while saving this template. Please try again later.'));
            $this->_getSession()->setData('newsletter_template_form_data', $this->getRequest()->getParams());
        }
        $this->_forward('new');
    }

}