<?php
include Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Newsletter' . DS . 'QueueController.php';

class Poort24_Newsletter_Newsletter_QueueController extends Mage_Adminhtml_Newsletter_QueueController
{

    public function saveAction()
    {
        try {
            /* @var $queue Mage_Newsletter_Model_Queue */
            $queue = Mage::getModel('newsletter/queue');

            $templateId = $this->getRequest()->getParam('template_id');
            if ($templateId) {
                /* @var $template Mage_Newsletter_Model_Template */
                $template = Mage::getModel('newsletter/template')->load($templateId);

                if (!$template->getId() || $template->getIsSystem()) {
                    Mage::throwException($this->__('Wrong newsletter template.'));
                }

                $queue->setTemplateId($template->getId())
                    ->setQueueStatus(Mage_Newsletter_Model_Queue::STATUS_NEVER);
            } else {
                $queue->load($this->getRequest()->getParam('id'));
            }

            if (!in_array($queue->getQueueStatus(),
                array(Mage_Newsletter_Model_Queue::STATUS_NEVER,
                    Mage_Newsletter_Model_Queue::STATUS_PAUSE))
            ) {
                $this->_redirect('*/*');
                return;
            }

            if ($queue->getQueueStatus() == Mage_Newsletter_Model_Queue::STATUS_NEVER) {
                $queue->setQueueStartAtByString($this->getRequest()->getParam('start_at'));
            }

            $customerUsers = $this->getRequest()->getParam('customer_user_ids');
            $customerGroups = $this->getRequest()->getParam('customer_group_ids', array());

            if ($all = $this->getRequest()->getParam('customer_group_all', '')) {
                if ($all == 'all') {
                    $customerGroups = array();
                    foreach (Mage::getResourceModel('customer/group_collection')->load()->toOptionArray() as $pair) {
                        $customerGroups[] = $pair['value'];
                    }
                } else {
                    $customerGroups = array();
                }
            }

            $queue->setStores($this->getRequest()->getParam('stores', array()))
                ->setNewsletterSubject($this->getRequest()->getParam('subject'))
                ->setNewsletterSenderName($this->getRequest()->getParam('sender_name'))
                ->setNewsletterSenderEmail($this->getRequest()->getParam('sender_email'))
                ->setNewsletterText($this->getRequest()->getParam('text'))
                ->setNewsletterStyles($this->getRequest()->getParam('styles'))
                ->setCustomerGroups($customerGroups)
                ->setCustomerUsers(preg_split('/[\n\r]+/', $customerUsers));

            if ($queue->getQueueStatus() == Mage_Newsletter_Model_Queue::STATUS_PAUSE
                && $this->getRequest()->getParam('_resume', false)
            ) {
                $queue->setQueueStatus(Mage_Newsletter_Model_Queue::STATUS_SENDING);
            }

            $queue->save();

            if ($id = $this->getRequest()->getParam('id')) {
                $this->_redirect('*/*/edit', array('id' => $id));
            } else {
                $this->_redirect('*/*');
            }
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $this->_redirect('*/*/edit', array('id' => $id));
            } else {
                $this->_redirectReferer();
            }
        }
    }

}