<?php

class Poort24_Newsletter_Model_QueueGroupLink extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('poort24_newsletter/queueGroupLink');
    }

}