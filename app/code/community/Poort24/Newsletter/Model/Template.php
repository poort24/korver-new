<?php

class Poort24_Newsletter_Model_Template extends Mage_Newsletter_Model_Template
{
    protected $_oldDesignData = null;
    protected $_oldStore = null;
    protected $_modifiedDesignData = array(
        'theme' => 'korver',
        'area' => 'frontend',
        'package' => 'korver',
        'store' => 1,
    );

    protected $_products = array();

    public function getProcessedTemplate(array $variables = array(), $usePreprocess = false)
    {
        $this->_prepareDesign();
        $variables['customer'] = null;
        if (!empty ($variables['subscriber'])) {
            if ($customerId = $variables['subscriber']->getCustomerId()) {
                $variables['customer'] = Mage::getModel('customer/customer')->load($customerId);
            }
        }
        $variables['products'] = array();
        $variables['mail'] = $this;
        foreach ($this->getTemplateProducts() as $sku) {
            $sku = trim($sku);
            if (isset ($this->_products[$sku])) {
                $variables['products'][$sku] = $this->_products[$sku];
            } else if ($product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku)) {
                $this->_products[$sku] = $product;
                $variables['products'][$sku] = $product;
            }
        }
        $text = parent::getProcessedTemplate($variables, false); // no preprocessing
        $this->_restoreDesign();
        return $text;
    }

    public function send($subscriber, array $variables = array(), $name = null, Mage_Newsletter_Model_Queue $queue = null)
    {
        if (!$this->isValidForSend()) {
            return false;
        }

        if ($subscriber instanceof Mage_Newsletter_Model_Subscriber) {
            $email = $subscriber->getSubscriberEmail();
        } else {
            $email = (string)$subscriber;
            $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);

        }

        $name = $email;
        if (is_null($name) && ($subscriber->hasCustomerFirstname() || $subscriber->hasCustomerLastname())) {
            $name = $subscriber->getCustomerFirstname() . ' ' . $subscriber->getCustomerLastname();
        }

        if (Mage::getStoreConfigFlag(Mage_Newsletter_Model_Subscriber::XML_PATH_SENDING_SET_RETURN_PATH)) {
            $this->getMail()->setReturnPath($this->getTemplateSenderEmail());
        }

        ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
        ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

        $mail = $this->getMail();

        $mail->addTo($email, $name);
        $text = $this->getProcessedTemplate(array(), true);

        if ($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        $mail->setSubject($this->getProcessedTemplateSubject($variables));
        $mail->setFrom($this->getTemplateSenderEmail(), $this->getTemplateSenderName());

        if ($helper = Mage::helper('poort24_newsletter')) {
            $helper->writeNewsletter($text, $this, $mail, $subscriber);
        }

        try {
            if ($subscriber instanceof Mage_Newsletter_Model_Subscriber && $subscriber->isSubscribed()) {
                if ($subscriber->getSubscriberStatus() != 1) {
                    return false;
                }

                $mail->send();

                if (!is_null($queue) && $subscriber instanceof Mage_Newsletter_Model_Subscriber) {
                    $subscriber->received($queue);
                }
            }

            $this->_mail = null;
        } catch (Exception $e) {
            if ($subscriber instanceof Mage_Newsletter_Model_Subscriber) {
                $problem = Mage::getModel('newsletter/problem');
                $problem->addSubscriberData($subscriber);
                if (!is_null($queue)) {
                    $problem->addQueueData($queue);
                }
                $problem->addErrorData($e);
                $problem->save();

                if (!is_null($queue)) {
                    $subscriber->received($queue);
                }
            } else {
                throw $e;
            }
            return false;
        }

        return true;
    }

    public function getTemplateProducts()
    {
        $data = $this->getData('template_products');
        if ($data) {
            return unserialize($data);
        }
        return array();
    }

    public function setTemplateProducts($data)
    {
        if (!$data || !is_array($data)) {
            $data = array();
        }
        $data = array_unique(array_filter($data));
        return $this->setData('template_products', serialize($data));
    }

    public function setTemplateChoice($choice)
    {
        $layout = Mage::app()->getLayout();
        $this->_prepareDesign();
        $block = $layout->createBlock('core/template', null, array(
            'template' => "newsletter/mail/{$choice}.phtml",
        ));
        $html = $block->toHtml();
        $this->_restoreDesign();
        $this->setTemplateText($html);
        return $this->setData('template_choice', $choice);
    }

    protected function _prepareDesign()
    {
        $design = Mage::getDesign();

        $this->_oldStore = $design->getStore();
        $this->_oldDesignData = array(
            'theme' => $design->getTheme('template'),
            'area' => $design->getArea(),
            'package' => $design->getPackageName(),
            'store' => $design->getStore(),
        );
        $this->_setDesign($this->_modifiedDesignData);
        Mage::app()->setCurrentStore(Mage::getDesign()->getStore());
    }

    protected function _restoreDesign()
    {
        if ($this->_oldDesignData) {
            $this->_setDesign($this->_oldDesignData);
            Mage::app()->setCurrentStore($this->_oldStore);
        }
    }

    protected function _setDesign(array $data)
    {
        $design = Mage::getDesign();
        $design->setTheme($data['theme']);
        $oldArea = $design->getArea();
        $design->setArea($data['area']);
        $oldPackage = $design->getPackageName();
        $design->setPackageName($data['package']);
        $oldStore = $design->getStore();
        $design->setStore($data['store']);
    }

}