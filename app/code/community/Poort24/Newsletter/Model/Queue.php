<?php

class Poort24_Newsletter_Model_Queue extends Mage_Newsletter_Model_Queue
{

    protected function _construct()
    {
        $this->_init('poort24_newsletter/queue');
    }

}