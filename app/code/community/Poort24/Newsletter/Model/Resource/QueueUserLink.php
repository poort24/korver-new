<?php

class Poort24_Newsletter_Model_Resource_QueueUserLink extends Mage_Core_Model_Mysql4_Abstract
{

    protected function _construct()
    {
        $this->_init('poort24_newsletter/queueUserLink', 'link_id');
    }

}