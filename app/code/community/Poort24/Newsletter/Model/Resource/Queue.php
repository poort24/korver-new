<?php

class Poort24_Newsletter_Model_Resource_Queue extends Mage_Newsletter_Model_Mysql4_Queue
{

    public function setStores(Mage_Newsletter_Model_Queue $queue)
    {
        $this->storeGroupLinks($queue);
        $this->storeUserLinks($queue);

        $this->_getWriteAdapter()
            ->delete(
                $this->getTable('queue_store_link'),
                $this->_getWriteAdapter()->quoteInto('queue_id = ?', $queue->getId())
            );

        if (!is_array($queue->getStores())) {
            $stores = array();
        } else {
            $stores = $queue->getStores();
        }

        foreach ($stores as $storeId) {
            $data = array();
            $data['store_id'] = $storeId;
            $data['queue_id'] = $queue->getId();
            $this->_getWriteAdapter()->insert($this->getTable('queue_store_link'), $data);
        }

        $this->removeSubscribersFromQueue($queue);

        if (count($stores) == 0) {
            return $this;
        }

        $subscribers = Mage::getResourceSingleton('newsletter/subscriber_collection');
        $customerTable = Mage::getSingleton('core/resource')->getTableName('customer_entity');
        $userIds = false;
        $debtors = false;

        if ($users = $queue->getCustomerUsers()) {
            $userIds = array();
            $debtors = array();

            foreach ($users as $user) {
                if ($user != '') {
                    if ($this->isDebiteurNr($user)) {
                        $debtors[] = '"' . strtoupper($user) . '"';
                    } else {
                        $userIds[] = $user;
                    }
                }
            }

            $userIds = implode(", ", array_map('abs', $userIds));
            $debtors = implode(", ", $debtors);

            if ($userIds) {
                $subscribers->getSelect()->where("main_table.customer_id IN(" . $userIds . ")");
            } elseif ($debtors) {
                $subscribers->getSelect()
                    ->joinLeft(array('customer' => $customerTable . '_varchar'), "customer.entity_id = main_table.customer_id", array('attribute_id', 'value'))
                    ->join(array('eav' => 'eav_attribute'), "eav.attribute_id = customer.attribute_id AND eav.attribute_code = 'korver_debiteur_nr'")
                    ->where("(UPPER(customer.value) IN(" . $debtors . "))");
            }
        }

        if (!$userIds &&
            !$debtors &&
            $groups = $queue->getCustomerGroups()
        ) {
            $nullGroup = in_array(0, $groups);
            $groups[] = 0;
            $groups = implode(", ", array_map('abs', $groups));
            $subscribers->getSelect()
                ->joinLeft(array('customer' => $customerTable), 'customer.entity_id = main_table.customer_id', array('group_id'));

            if ($nullGroup) {
                $subscribers->getSelect()->where("customer.group_id IN(" . $groups . ") OR customer.group_id");
            } else {
                $subscribers->getSelect()->where("customer.group_id IN(" . $groups . ") OR (customer.entity_id IS NOT NULL AND customer.group_id IS NULL)");
            }
        }

        $subscribers
            ->addFieldToFilter('store_id', array('in' => $stores))
            ->useOnlySubscribed()
            ->load();
        $subscriberIds = array();

        foreach ($subscribers as $subscriber) {
            $subscriberIds[] = $subscriber->getId();
        }

        if (count($subscriberIds) > 0) {
            $this->addSubscribersToQueue($queue, $subscriberIds);
        }

        return $this;
    }

    protected function storeGroupLinks(Mage_Core_Model_Abstract $queue)
    {
        $collection = Mage::getModel('poort24_newsletter/queueGroupLink')->getCollection();
        $collection->setQueueId($queue->getId())->delete();

        if (is_array($queue->getCustomerGroups())) {
            if (in_array('all', $queue->getCustomerGroups())) {
                $groups = Mage::getModel('customer/group');
                $groupIds = array();
                foreach ($groups->getCollection()->load() as $g) {
                    $groupIds[] = $g->getId();
                }
                $queue->setCustomerGroups($groupIds);
            }
        }

        foreach ($queue->getCustomerGroups() as $group) {
            $m = $collection->getNewEmptyItem();
            $m->setGroupId($group)->setQueueId($queue->getId())->save();
        }
        return $this;
    }

    protected function storeUserLinks(Mage_Core_Model_Abstract $queue)
    {
        $collection = Mage::getModel('poort24_newsletter/queueUserLink')->getCollection();
        $collection->setQueueId($queue->getId())->delete();

        if (is_array($queue->getCustomerUsers())) {
            foreach ($queue->getCustomerUsers() as $user) {
                $new_item = $collection->getNewEmptyItem();

                if ($user != '') {
                    $new_item->setUser(trim($user))->setType($this->isDebiteurNr($user) ? 'd' : 'm')->setQueueId($queue->getId())->save();
                }
            }
        }
        return $this;
    }

    protected function isDebiteurNr($user)
    {
        return substr(trim(strtolower($user)), 0, 1) == 'd';
    }

}