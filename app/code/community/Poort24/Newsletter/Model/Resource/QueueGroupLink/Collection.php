<?php

class Poort24_Newsletter_Model_Resource_QueueGroupLink_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('poort24_newsletter/queueGroupLink');
    }

    function setQueueId($queueId)
    {
        return $this->addFieldToFilter('queue_id', $queueId);
    }

    public function delete()
    {
        $ids = $this->getAllIds();
        $ids[] = 0;
        $ids = implode(", ", array_map('abs', $ids));
        $table = $this->getResource()->getMainTable();
        $db = Mage::getSingleton("core/resource")->getConnection("newslettergroups_write");
        $db->query("DELETE FROM `{$table}` WHERE link_id IN($ids)");
    }

}