<?php

class Poort24_Newsletter_Model_Resource_Queue_Collection extends Mage_Newsletter_Model_Mysql4_Queue_Collection
{

    protected function _construct()
    {
        $this->_init('poort24_newsletter/queue');
    }

}