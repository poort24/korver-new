<?php

class Poort24_Newsletter_Block_Template_Edit extends Mage_Adminhtml_Block_Newsletter_Template_Edit
{

    public function renderView()
    {
        $html = parent::renderView();
        $model = $this->getModel();
        if ($model && $model->getId()) {
            /*
             * show previews using the ID, if possible
             * this makes sure fields like template_intro etc. are available in the template
             * otherwise, the template text is sent to the preview frame in a POST variable, and other values are not sent
             */
            $html = preg_replace("(id=\"newsletter_template_preview_form\"[^>]*>)s", "\\0<input type=\"hidden\" id=\"preview_id\" name=\"id\" value=\"{$this->getModel()->getId()}\" />", $html);
        }
        return $html;
    }

    protected function _prepareLayout()
    {
        // added this code
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        parent::_prepareLayout();
    }

}