<?php

class Poort24_Newsletter_Block_Template_Edit_Form extends Mage_Adminhtml_Block_Newsletter_Template_Edit_Form
{
    protected $_default_template = 'korverholland';

    protected function _prepareForm()
    {
        /* @var Poort24_Newsletter_Model_Template */
        $template = $this->getModel();
        $helper = Mage::helper('poort24_newsletter');

        if (!$template->getTemplateChoice()) {
            $template->setTemplateChoice($this->_default_template);
        }

        $configSettings = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array(
                'add_widgets' => false,
                'add_variables' => false,
                'add_images' => false,
                'files_browser_window_url' => $this->getBaseUrl() . 'admin/cms_wysiwyg_images/index/',
            ));

        parent::_prepareForm();

        $form = $this->getForm();

        if ($form->getElement('code')->getValue() === null) {
            $form->getElement('sender_name')->setValue('');
            $form->getElement('sender_email')->setValue('');
        }

        $content1 = $template->getTemplateContent1();
        $content2 = $template->getTemplateContent2();
        $content3 = $template->getTemplateContent3();
        $description1 = $template->getTemplateDescription1();
        if (!$template->getId()) {
            $content1 = $content2 = $content3 = $helper->getDefaultContent();
            $description1 = '';
        }

        $form->getElement('text')
            ->setStyle('display: none')
            ->setRequired(false);

        $fieldset = $form->addFieldset('products_fieldset', array(
            'legend' => Mage::helper('poort24_newsletter')->__('Products'),
            'class' => 'fieldset-wide'
        ));

        $fieldset->addField('newsletter_expires', 'date', array(
            'name' => 'newsletter_expires',
            'label' => Mage::helper('poort24_newsletter')->__('Valid until'),
            'title' => Mage::helper('poort24_newsletter')->__('Valid until'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'time' => false,
            'value' => $template->getNewsletterExpires(),
        ));

        $fieldset->addField('template_choice', 'select', array(
            'name' => 'template_choice',
            'label' => Mage::helper('newsletter')->__('Design'),
            'title' => Mage::helper('newsletter')->__('Design'),
            'required' => false,
            'state' => 'html',
            'values' => $helper->getTemplates(),
            'value' => $template->getTemplateChoice(),
        ));

        $fieldset->addField('template_content1', 'editor', array(
            'name' => 'template_content1',
            'wysiwyg' => (!$template->isPlain()),
            'label' => Mage::helper('poort24_newsletter')->__('Intro text'),
            'title' => Mage::helper('poort24_newsletter')->__('Intro text'),
            'theme' => 'advanced',
            'required' => false,
            'state' => 'html',
            'style' => 'height:36em;',
            'value' => $content1,
            'config' => $configSettings
        ));

        $fieldset->addField('products', 'editor', array(
            'name' => 'products',
            'wysiwyg' => false,
            'label' => Mage::helper('poort24_newsletter')->__('Products (SKU)'),
            'title' => Mage::helper('poort24_newsletter')->__('Products (SKU)'),
            'theme' => 'advanced',
            'state' => 'html',
            'style' => 'height:20em;',
            'value' => implode($template->getTemplateProducts(), "\n"),
            'config' => $configSettings
        ));

        $fieldset->addField('template_content2', 'editor', array(
            'name' => 'template_content2',
            'wysiwyg' => (!$template->isPlain()),
            'label' => Mage::helper('poort24_newsletter')->__('Closing text'),
            'title' => Mage::helper('poort24_newsletter')->__('Closing text'),
            'theme' => 'advanced',
            'required' => false,
            'state' => 'html',
            'style' => 'height:36em;',
            'value' => $content2,
            'config' => $configSettings
        ));

        $fieldset->addField('template_content3', 'editor', array(
            'name' => 'template_content3',
            'wysiwyg' => (!$template->isPlain()),
            'label' => Mage::helper('poort24_newsletter')->__('Sidebar text'),
            'title' => Mage::helper('poort24_newsletter')->__('Sidebar text'),
            'theme' => 'advanced',
            'required' => false,
            'state' => 'html',
            'style' => 'height:36em;',
            'value' => $content3,
            'config' => $configSettings
        ));

        $fieldset->addField('template_description1', 'textarea', array(
            'name' => 'template_description1',
            'label' => Mage::helper('poort24_newsletter')->__('Text sales page'),
            'title' => Mage::helper('poort24_newsletter')->__('Text sales page'),
            'required' => false,
            'value' => $description1,
            'config' => $configSettings
        ));

        return $this;
    }

}