<?php

class Poort24_Newsletter_Block_Queue_Edit_Form extends Mage_Adminhtml_Block_Newsletter_Queue_Edit_Form
{
    /* @var Poort24_Newsletter_Helper_Data */
    protected $_helper;

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $this->_helper = Mage::helper('poort24_newsletter');

        parent::_prepareForm();

        $form = $this->getForm();
        $form->getElement('text')->setStyle('width:98%; height: 200px;');

        self::_prepareGroupForm($form);
        self::_prepareUserForm($form);

        return $this;
    }

    /**
     * @param Varien_Data_Form $form
     */
    protected function _prepareGroupForm($form)
    {
        $fieldset = $form->addFieldset('groups_fieldset', array(
            'legend' => $this->_helper->__('Customer groups'),
            'class' => 'fieldset-wide'
        ));

        $queue = Mage::getSingleton('newsletter/queue');
        $customerGroups = Mage::getResourceModel('customer/group_collection')
            ->load()->toOptionArray();

        $checked = array();
        if ($queueId = $queue->getId()) {
            $group_links = Mage::getModel('poort24_newsletter/queueGroupLink')->getCollection()->setQueueId($queue->getId())->load();
            foreach ($group_links as $group_link) {
                $checked[$group_link->getGroupId()] = $group_link->getGroupId();
            }
        }

        $fieldset->addField('customer_group_all', 'select', array(
            'name' => 'customer_group_all',
            'label' => $this->_helper->__('Select'),
            'title' => $this->_helper->__('Select'),
            'values' => array('' => '', 'all' => $this->_helper->__('Select all'), 'none' => $this->_helper->__('Deselect all')),
        ));

        $fieldset->addField('customer_group_ids', 'checkboxes', array(
            'name' => 'customer_group_ids[]',
            'label' => $this->_helper->__('Customer Groups'),
            'title' => $this->_helper->__('Customer Groups'),
            'values' => $customerGroups,
            'checked' => $checked
        ));
    }

    /**
     * @param Varien_Data_Form $form
     */
    protected function _prepareUserForm($form)
    {
        $fieldset = $form->addFieldset('users_fieldset', array(
            'legend' => $this->_helper->__('Customers'),
            'class' => 'fieldset-wide'
        ));

        $queue = Mage::getSingleton('newsletter/queue');

        $users = array();

        if ($queueId = $queue->getId()) {
            $user_links = Mage::getModel('poort24_newsletter/queueUserLink')->getCollection()->setQueueId($queue->getId())->load();
            foreach ($user_links as $user_link) {
                $users[] = $user_link->getUser();
            }
        }
        $users = implode("\n", $users);

        $fieldset->addField('customer_user_ids', 'textarea', array(
            'name' => 'customer_user_ids',
            'label' => $this->_helper->__('Customers'),
            'title' => $this->_helper->__('Customers'),
            'value' => $users
        ));
    }

}