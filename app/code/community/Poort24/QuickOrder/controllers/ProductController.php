<?php

class Poort24_QuickOrder_ProductController extends Mage_Core_Controller_Front_Action
{
    protected $_session;

    public function preDispatch()
    {
        Mage::getSingleton('core/session', array('name' => 'frontend'));
        $this->_session = Mage::getSingleton('customer/session');

        if (!isset($this->storage) || is_null($this->storage)) {
            $this->storage = array();
        }
    }

    public function searchAction()
    {
        $result = '';
        $sku = '';

        $data = current($this->getRequest()->getPost('quickorder'));
        if (array_key_exists('sku', $data)) {
            $sku = trim($data['sku']);
        }

        $sku_value = '%' . $sku . '%';
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter(array(
                array('attribute' => 'name', 'like' => $sku_value),
                array('attribute' => 'description', 'like' => $sku_value),
                array('attribute' => 'sku', 'like' => $sku_value)
            ));
        $products->getSelect()->limit(120);

        foreach ($products as $product) {
            if ($product && $product->isSalable() && $product->getStatus() != '2') {
                $result .= "<div id='" . $product->getSku() . "'>" . $product->getSku() . " - " . $product->getName() . "</div>";
            }
        }

        echo json_encode(array($result, $sku));
    }

    public function descriptionAction()
    {
        if (!$this->_session || !$this->_session->isLoggedIn()) {
            echo json_encode(array('error', '', 'no session'));
            die;
        }

        // only send prices if the customer has permission to view them
        $canViewPrices = true; // Mage::helper('exed_customerPermissions')->viewPrices();

        if ($this->getRequest()->isPost()) {
            $data = current($this->getRequest()->getPost('quickorder'));
            if (array_key_exists('sku', $data)) {
                $sku = trim($data['sku']);
                $qty = intval($data['qty']);

                if (!$sku) {
                    return;
                }

                $product_coll = Mage::getResourceModel('catalog/product_collection')
                    ->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addAttributeToFilter('sku', $sku);
                $product = Mage::getModel('catalog/product')->load($product_coll->getFirstItem()->getId());

                if ($product && $product->getTypeId() == 'simple' && $product->isSalable() && $product->getStatus() != '2') {
                    $priceModel = $product->getPriceModel();
                    $price = $product->getFinalPrice($qty);
//                    $priceSource = $priceModel->getPriceSource();
                    $baseprice = $priceModel->getBasePrice($product, 1);
                    $image_url = (string)Mage::helper('catalog/image')->init($product, 'image');

                    $korting = '-';
//                    if ($priceSource == 'discount') {
//                        $kortingPercentage = $priceModel->getDiscountPercentage();
//                        if ($kortingPercentage > 0) {
//                            $korting = number_format($kortingPercentage, 1, ',', '.') . '%';
//                        }
//                    } else if ($priceSource == 'temp') {
//                        $korting = 'Actieprijs';
//                    }

                    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                    $stockQty = intval($stock->getQty());
                    $inStock = (($stockQty > 0) || !$stock->getManageStock()); // && $product->getInVoorraad() !== 0;
                    $product->load($product->getId());
                    $actie = false;

//                    if ($product->getPriceModel()->getPriceSource() == 'magento_specialprice' || $product->getPriceModel()->getPriceSource() == 'magento_specialprice_pricerule') {
//                        $actie = true;
//                    } else if ($product->getPriceModel()->getPriceSource() == 'magento_pricerule' || $product->getPriceModel()->getPriceSource() == 'magento_specialprice_pricerule') {
//                        $actie = true;
//                    }
                    if ($product->getPrice() != $product->getFinalPrice($qty)) {
                        $actie = true;
                    }

                    $json = json_encode(
                        array(
                            'title',
                            $product->getName(),
                            array('in_stock' => $inStock, 'stock_qty' => $stockQty, 'over_stock' => ($stockQty < $qty) && $stock->getManageStock()),
                            $canViewPrices ? '&euro;' . number_format($price, 2) : '',
                            '&euro;' . number_format($baseprice, 2),
                            $canViewPrices ? $korting : '',
                            $this->_getPopupContent($product),
                            $image_url,
                            $product->getProductUrl(),
                            $actie,
                            $product->getData('delivery_date'),
                        )
                    );

                    $this->updateSession($sku, $qty, $json);
                    echo $json;
                } else if ($product && $product->getTypeId() == 'bundle' && (!$product->isSalable() || $product->getStatus() != '2')) {
                    echo json_encode(array('error', $product->getName(), 'bundle product', $product->getProductUrl()));
                } else if ($product && (!$product->isSalable() || $product->getStatus() != '2')) {
                    echo json_encode(array('error', $product->getName(), 'nt for sale'));
                } else {
                    echo json_encode(array('error', '', 'not savailable'));
                }

            }
        } else {
            $this->_redirectSuccess(Mage::getUrl('/'));
        }
    }

    public function checkAction()
    {
        if ($this->getRequest()->isPost() && $this->_session && $this->_session->isLoggedIn()) {
            $data = $this->getRequest()->getPost('quickorder');
            $response = array();
            $order = array();

            foreach ($data as $key => $item) {
                $key = str_replace('"', '', $key);
                $qty = 1;
                $sku = null;
                if (array_key_exists('sku', $item)) $sku = trim($item['sku']);
                if (array_key_exists('qty', $item)) $qty = $item['qty'];

                if (!$sku) {
                    continue;
                }

                $product_coll = Mage::getResourceModel('catalog/product_collection')
                    ->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addAttributeToFilter('sku', $sku);
                $product = Mage::getModel('catalog/product')->load($product_coll->getFirstItem()->getId());

                if ($product && $product->getTypeId() == 'simple' && $product->isSalable()) {
                    $order[] = array($product->getId(), $qty);
                } else if ($product && !$product->isSalable()) {
                    $response[] = array($key, 'not for sale');
                } else if ($product && $product->getTypeId() == 'bundle') {
                    //$response[] = array($key, 'bundle product');
                } else {
                    $response[] = array($key, 'not available');
                }
            }

            if (!sizeof($response) || sizeof($order)) {
                /** no errors have been found, place the order **/
                $cart = Mage::getSingleton('checkout/cart');

                foreach ($order as $orderline) {
                    $cart->addProduct((int)$orderline[0], array('qty' => $orderline[1]));
                }

                $cart->save();
                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
                $response[] = array('-', 'order is placed');
            }

            echo json_encode($response);
        } else {
            $this->_redirectSuccess(Mage::getUrl('/'));
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->isPost()) {
            $sku = $this->getRequest()->getPost('sku');
            if (!empty($sku)) {
                unset($this->storage[$sku]);
            }
        }
    }

    private function updateSession($sku, $qty, $json)
    {
        $this->storage[$sku] = array($sku, $qty, $json);
    }

    private function _getPopupContent($product)
    {
        $layout = Mage::app()->getLayout();
        $block = $layout->createBlock('core/template', null, array(
            'template' => "poort24/quickorder/popup.phtml",
        ));
        $block->setProduct($product);
        return $block->toHtml();
    }

}