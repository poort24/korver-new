<?php
$layout_update_xml = '
<reference name="head">
  <action method="addItem"><type>skin_js</type><name>js/poort24/quickorder.js</name></action>
  <action method="addItem"><type>skin_css</type><name>css/poort24/quickorder.css</name></action>
</reference>';

$cmsPageData = array(
    'title' => 'Bestel snel',
    'root_template' => 'two_columns_left',
    'meta_keywords' => 'bestel,snel,korverholland',
    'meta_description' => '',
    'identifier' => 'quick-order',
    'content_heading' => 'Bestel snel',
    'stores' => array(1),
    'content' => '{{block type="core/template" name="quickorder" template="poort24/quickorder/view.phtml"}}',
    'layout_update_xml' => $layout_update_xml
);

$cmspage = Mage::getModel('cms/page');
$cmspage->setData($cmsPageData);
$cmspage->save();

$cmsPageDataEn = array(
    'title' => 'Quick Order',
    'root_template' => 'two_columns_left',
    'meta_keywords' => 'quick,order,korverholland',
    'meta_description' => '',
    'identifier' => 'quick-order',
    'content_heading' => 'Quick Order',
    'stores' => array(2),
    'content' => '{{block type="core/template" name="quickorder" template="poort24/quickorder/view.phtml"}}',
    'layout_update_xml' => $layout_update_xml
);

$cmspageEn = Mage::getModel('cms/page');
$cmspageEn->setData($cmsPageDataEn);
$cmspageEn->save();