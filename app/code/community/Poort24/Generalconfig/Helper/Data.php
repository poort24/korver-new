<?php

/**
 * Created by PhpStorm.
 * User: bschut
 * Date: 03-08-16
 * Time: 12:59
 */
class Poort24_Generalconfig_Helper_Data extends Mage_Core_Helper_Abstract
{

    public static $counter = 0;

    /**
     * Get categories for left navigation on product view page
     *
     * @param $categories
     * @param null $node
     * @return string
     */
    public function getCategories($categories, $node = NULL)
    {
        $array = '<ul style="padding-left: ' . $this::$counter * 10 . 'px">';
        foreach ($categories as $category) {
            $cat = Mage::getModel('catalog/category')->load($category->getId());
            $count = $cat->getProductCount();

            if ($category->hasChildren()) {
                $this::$counter++;
                $children = Mage::getModel('catalog/category')->getCategories($category->getId());
                $childData = $this->getCategories($children, 'parent');
                $class = 'cat-parent';
            } else {
                $childData = '';
                $class = 'cat-child';
            }

            $catUrl = Mage::getBaseUrl () . $cat->getUrlPath();
            if(Mage::helper('core/url')->getCurrentUrl() == $catUrl) {
                $array .= '<li class=' . 'active-parent cat-parent' . '>' .
                    '<a href="' . $catUrl . '" class="' . 'active' . '">' . $category->getName() . "</a>\n";
            } else {
                $array .= '<li class=' . $class . '>' .
                    '<a href="' . $catUrl . '">' . $category->getName() . "</a>\n";
            }

            if($class === 'cat-parent') {
                $array .= '<span class="cat-toggler">
                                <svg style="display: block;" width="8px" height="11px" viewBox="0 0 6 9" version="1.1" class="toggler-plus">
                                    <g id="Ontwerp" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Category---Grid" transform="translate(-446.000000, -384.000000)" fill="#BAC8CB">
                                            <g id="Category" transform="translate(187.000000, 302.000000)">
                                                <g id="Triangle" transform="translate(257.000000, 82.000000)">
                                                    <polygon id="Triangle-1-Copy" transform="translate(5.259821, 4.500000) rotate(-90.000000) translate(-5.259821, -4.500000) " points="0.8 2 9.71964286 2 5.25982143 7"></polygon>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <svg style="display: none;" width="12px" height="6px" viewBox="0 0 10 5" version="1.1" class="toggler-min"><g id="Ontwerp" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Category---Grid" transform="translate(-444.000000, -417.000000)" fill="#BAC8CB"> <g id="Category" transform="translate(187.000000, 302.000000)"><g id="Triangle" transform="translate(257.000000, 82.000000)"><polygon id="Triangle-1-Copy" points="0.8 33 9.71964286 33 5.25982143 38"></polygon></g></g></g></g></svg>
                            </span>';
            }
            $array .= $childData;
            $array .= '</li>';
        }
        return $array . '</ul>';
    }

    public function getRootCategories()
    {
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('level', 2)
            ->addAttributeToFilter('is_active', 1);

        return $categories;
    }

    public function checkCompareList($product) {
        $compared   = false;
        $collection = Mage::helper('catalog/product_compare')->getItemCollection();

        foreach($collection as $compare_product) {
            if($compare_product->getId() === $product->getId()) {
                $compared = true;
            }
        }

        return $compared;
    }
}